<?php

use yii\db\Migration;

/**
 * Class m180121_225023_add_column_status
 */
class m180121_225023_add_column_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180121_225023_add_column_status cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('requests','status',$this->string());
    }

    public function down()
    {
        $this->dropColumn('requests','status');
    }

}

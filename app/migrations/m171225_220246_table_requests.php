<?php

use yii\db\Migration;

/**
 * Class m171225_220246_table_requests
 */
class m171225_220246_table_requests extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m171225_220246_table_requests cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('requests',['id' => $this->primaryKey(),
                                            'name' => $this->string(),
                                            'surname' => $this->string(),
                                            'phone_number' => $this->string(),
                                            'email' => $this->string()]);
    }

    public function down()
    {
        $this->dropTable('requests');
    }

}

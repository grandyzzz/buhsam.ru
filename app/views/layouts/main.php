<?php

?>
<?php $this->beginContent('@app/views/layouts/base.php'); ?>
    <div id="wrapper">
        <header>
            <div class="container">
                <div class="header__nav">
                    <a class="logo" href="/"><h1>Бухсам</h1></a>
                    <ul class="header__nav-menu">
                        <li><a href="">О нас</a></li>
                        <li><a href="">Что вы получите</a></li>
                        <li><a href="">Контакты</a></li>
                        <li class="tel"><a href="tel:386300">386-300</a></li>
                    </ul>
                </div>
            </div>   
        </header>
        
        <main>
            <?= $content ?>
        </main>

        <footer>
            <div class="container">
                <div class="footer__flex-container">
                    <ul class="header__nav-menu">
                        <li><a href="/about">О нас</a></li>
                        <li><a href="/">Что вы получите</a></li>
                        <li><a href="/contacts">Контакты</a></li>
                    </ul>
                    <div class="footer__contacts">
                        <ul class="footer__contacts__social">
                            <? foreach (\grozzzny\soc_link\models\SocLink::find()->all() as $item):?>
                                <li>
                                    <a href="<?=$item->link?>" title="<?=$item->name?>">
                                        <i class="<?=$item->icon?>"></i>
                                    </a>
                                </li>
                            <? endforeach;?>
                        </ul>
                        <ul class="header__nav-menu">
                            <li><a href="tel:386300">386-300</a></li>
                            <li><a href="mailto:buchsam@gmail.com">buchsam@gmail.com</a></li>
                            <li><a href="/contacts">г. Калининград, ул. Садовая д. 8, оф. 4</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
<?php $this->endContent(); ?>
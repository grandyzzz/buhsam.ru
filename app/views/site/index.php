<?php
use yii\bootstrap\Alert;
use yii\easyii\helpers\Image;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

$this->title = 'EasyiiCMS start page';
?>

<section class="section__intro" data-src="<?= Image::thumb('/app/media/images/bg.jpg')?>" data-parallax>
    <div class="container">

        <div class="section__intro-content">
            <h1>Делаем бизнес проще</h1>
            <p>Проведем аудит и разработаем оптимальную систему
                бухгалтерского учета и налогообложения, чтобы Вы
                сосредоточились на главном - развитии своего бизнеса.</p>
        </div>
        <div class="section__intro-form">
            <a href="" class="btn__rounded" data-toggle="modal" data-target="#exampleModalCenter">
                Заказать обратный звонок
            </a>
        </div>
    </div>
</section>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <h2 align="center">Мы с радостью на ответим Вам на интересующие вопросы!</h2>
          <?  $form = ActiveForm::begin([
            'id' => 'callback-form',
            'options' => ['class' => 'form__callback'],
            ]) ?>
            <?= $form->field($model, 'name')->label(false)->textInput(['placeholder' => $model->getAttributeLabel('name')]) ?>
            <?= $form->field($model, 'phone_number')->label(false)->widget(\yii\widgets\MaskedInput::className(), [
                'mask' => '+7(999)999-99-99',
                'clientOptions'=>[
                    'clearIncomplete'=>true,
                    'removeMaskOnSubmit' => true,
                ]
            ])->textInput(['placeholder' => $model->getAttributeLabel('phone_number')]) ?>



                    <?= Html::submitButton('Отправить', ['class' => 'btn__submit']) ?>


            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
<section class="section__skills">
    <div class="container">
        <div class="section__skills-title">
            <h1>Что мы можем</h1>
            <p>Не отвлекайтесь по пустякам и просто
                доверьтесь профессионалам.</p>
        </div>
        <div class="section__skills-content">
            <div class="row">
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Сдаем отчетность
                            вовремя и без ошибок</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Безопасно
                            снижаем налоги</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Регистрация и
                            ликвидация бизнеса</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Грамотно ведем
                            ВСЮ бухгалтерию</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Автоматизация
                            документооборота</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Представительство
                            в надзорных органах</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Кадровый учет
                            и делопроизводство</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Помогаем получить
                            льготы от государства</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Проверяем
                            контрагентов</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Готовим компанию к
                            оформлению кредита</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Экономический
                            аудит бизнеса</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="section__skills_item">
                        <img src="<?= Image::thumb('/app/media/images/user.png',128)?>" alt="">
                        <p>Оптимизация финансовой
                            деятельности</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section__target">
    <div class="section__target-title" data-src="<?= Image::thumb('/app/media/images/1.jpg')?>" data-parallax>
        <h1>Для кого работаем</h1>
    </div>
    <div class="section__target-content__container">
        <div class="container">
            <div class="section__target-content">
                <p>Вы варите кофе, шьёте одежду, продаёте
                    оборудование или материалы, делаете сайты
                    или рекламу - работаем с любой отраслью.</p>
                <p>Если у вас ещё не было бухгалтера, мы станем
                    первым. Поможем зарегистрировать компанию,
                    выберем систему налогообложения и
                    расскажем, как не платить слишком много
                    налогов.</p>
                <p>Если у вас есть бухгалтер, но вы решили его
                    сменить или оптимизировать бухгалтерские
                    затраты - аккуратно примем базу, проверим
                    ошибки и поможем восстановить учёт.</p>
            </div>
        </div>
    </div>
</section>
<section class="section__beginning" data-src="<?= Image::thumb('/app/media/images/2.jpg')?>" data-parallax>
    <h1 align="center">C чего начать?</h1>
    <div class="section__beginning-steps">
        <div class="section__beginning-step">
            <span>1</span>
            <p>
                Оставьте заявку на нашем сайте, задавайте
                любые вопросы и получайте оперативные
                консультации.
            </p>
        </div>
        <div class="section__beginning-step">
            <span>2</span>
            <p>
                Мы проверим информацию, проведем аудит
                вашего бизнеса, определим список задач и
                результат, который Вы в итоге получите.
            </p>
        </div>
        <div class="section__beginning-step">
            <span>3</span>
            <p>
                Всё! После этого Вы можете забыть про
                бухгалтерию и сосредоточиться на развитии своей
                компании.
            </p>
        </div>
    </div>
</section>
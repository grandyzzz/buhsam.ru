<?php
use yii\helpers\StringHelper;
use yii\helpers\Url;
use app\modules\requests\models\Feedback;

$this->title = Yii::t('easyii/requests', 'Feedback');
$module = $this->context->module->id;
?>

<?= $this->render('_menu') ?>

<?php if($data->count > 0) : ?>
    <table class="table table-hover">
        <thead>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <th width="50">#</th>
                <?php endif; ?>
                <th>Имя</th>
                <th width="150">Номер телефона</th>

            </tr>
        </thead>
        <tbody>
    <?php foreach($data->models as $item) : ?>
            <tr>
                <?php if(IS_ROOT) : ?>
                    <td><?= $item->id ?></td>
                <?php endif; ?>
                <td><?= $item->name ?></td>
                <td><?= $item->phone_number ?></td>
            </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
    <?= yii\widgets\LinkPager::widget([
        'pagination' => $data->pagination
    ]) ?>
<?php else : ?>
    <p><?= Yii::t('easyii', 'No records found') ?></p>
<?php endif; ?>
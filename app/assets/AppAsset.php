<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/bootstrap.min.css',
        'css/normalize.css',
        'css/styles.css'
    ];
    public $js = [
        'js/bootstrap.min.js',
        'js/scripts.js',
        'js/jquery.parallax.min.js',
        'https://use.fontawesome.com/releases/v5.0.4/js/all.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
